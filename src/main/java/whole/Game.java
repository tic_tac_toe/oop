/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package whole;

import java.util.Scanner;

public class Game {
    private Table t;
    private Player p1,p2,cp;
    
    public void welcome(){
        System.out.print("Welcome to XO! \n" );
    }
    
    public void bye(){
        System.out.print("Thank you for playing!" );
    }
    
    public void Psetting() {
        System.out.print("Player 1 please enter your name : ");
        Scanner ss = new Scanner(System.in);
        String n1 = ss.next();
        p1.setPlayer(n1,'X');
        System.out.print("Player 2 please enter your name : ");
        String n2 = ss.next();
        p2.setPlayer(n2,'O');
        cp = p2;
        System.out.print(p1.Name() + "be play as X, and " + p2.Name() + "will play as O.");
    }
    
    public void turn() {
        if (cp == p1){
            cp = p2;
        }else{
            cp = p1;
        }
    }
    
    public void play() {
        Psetting();
        welcome();
        Scanner ss = new Scanner(System.in);
        boolean playAgain = true;

        while (playAgain) {
            t.Table();
            String end = null;
            while (end == null){
                turn();
                t.showTable();
                t.Write(cp);
                end = t.checkWin(cp);   
            }
            t.showTable();
            System.out.println(end);  

            } 
            System.out.print("Play again? (yes/no): ");
            String Again = ss.next().toLowerCase();

            if (!Again.equals("yes")) {
                playAgain = false;
            }
            bye();
        }
}
