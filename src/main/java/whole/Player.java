/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package whole;

public class Player {
    private String name;
    private char mark;
    
    public void setPlayer(String name,char mark){
        this.name = name;
        this.mark = mark;
    }
    
    public String Name(){
        return name;
    }
    
    public char Mark(){
        return mark;
    }

}
