/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package whole;

import java.util.Scanner;


class Table {
    private char[] table;
    private Player p1,p2;
    Scanner ss = new Scanner(System.in);
    

    public void Table() {
        for (int i = 1; i < 10; i++) {
            table[i] = (char)i;
    }
}
    
    public void showTable() {
        System.out.println(table[7]+" "+table[8]+" "+table[9]);
        System.out.println(table[4]+" "+table[5]+" "+table[6]);
        System.out.println(table[1]+" "+table[2]+" "+table[3]);
    }

    public void Write(Player cp) {
        boolean pass = false;
        System.out.print("Please pick a slot with your numpad : ");
        char slot = ss.next().charAt(0);
        while (pass != true){
            try {int inp = Character.getNumericValue(slot);
                if (!(inp >= 1 && inp <= 9)) {
                    System.out.print("Invalid input. Please try again. \n");
                }else if(table[inp] == inp){
                    table[inp]= cp.Mark();
                    pass = true;
                }else{
                    System.out.println("Slot is taken. Please try again.");
                    
                }
            }catch (NumberFormatException e) {
                System.out.print("Invalid input. Please try again. \n");
                continue;
            }
        }
    }

    public String checkWin(Player cp) {
        for (int c = 0; c < 8; c++) {
            boolean win = false;
            switch (c) {
                case 0:
                    if(table[1] == table[2] && table[2]== table[3]){
                        win = true;
                    }
                    break;
                case 1:
                    if(table[4] == table[5] && table[5] == table[6]){
                        win = true;
                    }
                    break;
                case 2:
                    if(table[7] == table[8] && table[8] == table[9]){
                        win = true;
                    }
                    break;
                case 3:
                    if(table[1] == table[4] && table[4] == table[7]){
                        win = true;
                    }
                    break;
                case 4:
                    if(table[2] == table[5] && table[5] == table[8]){
                        win = true;
                    }
                    break;
                case 5:
                    if(table[3] == table[6] && table[6] == table[9]){
                        win = true;
                    }
                    break;
                case 6:
                    if(table[1] == table[5] && table[5] == table[9]){
                        win = true;
                    }
                    break;
                case 7:
                    if(table[3] == table[5] && table[5] == table[7]){
                        win = true;
                    }
                    break;
            }
            if (win == true && cp.Mark() == 'X') {
                return "Congratulations! X wins!";
            }
            else if (win == true && cp.Mark() == 'O') {
                return "Congratulations! O wins!";
            }
        }
        for (int i = 0; i < 9; i++) {
            if (table[i] == (char)i) {
                break;
            }
            else if (i > 9) {
                return "It’s a draw!";
            }
        }
        return null;
    }
}